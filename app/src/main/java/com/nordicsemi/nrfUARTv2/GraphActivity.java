package com.nordicsemi.nrfUARTv2;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

public class GraphActivity extends Activity {
    public static final String TAG = "nRFUART_GRAPH";

    public static GraphView graph_1, graph_2, graph_3, graph_4, graph_5, graph_6, graph_7, graph_8;
    public static int gphX_1, gphX_2, gphX_3, gphX_4, gphX_5, gphX_6, gphX_7, gphX_8;

    public static LineGraphSeries<DataPoint> series_1, series_2, series_3, series_4, series_5, series_6, series_7, series_8;
    public static LineGraphSeries<DataPoint> series[];
    private int lastX = 0;
    private int yAxisMin, yAxisMax;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        setGUI();

    }

    public void setGUI() {
        graph_1 = (GraphView) findViewById(R.id.graph_1);
        graph_2 = (GraphView) findViewById(R.id.graph_2);
        graph_3 = (GraphView) findViewById(R.id.graph_3);
        graph_4 = (GraphView) findViewById(R.id.graph_4);
//        graph_5 = (GraphView) findViewById(R.id.graph_5);
//        graph_6 = (GraphView) findViewById(R.id.graph_6);
//        graph_7 = (GraphView) findViewById(R.id.graph_7);
//        graph_8 = (GraphView) findViewById(R.id.graph_8);

        yAxisMin = getPreferences(Context.MODE_PRIVATE).getInt("YaxisMin", 0);
        yAxisMax = getPreferences(Context.MODE_PRIVATE).getInt("YaxisMax", 4095);
        
        setGraph();
    }

    private void setGraph() {
        lastX = gphX_1 = gphX_2 = gphX_3 = gphX_4 = gphX_5 = gphX_6 = gphX_7 = gphX_8 = 0;

        graph_1.removeAllSeries();
        graph_2.removeAllSeries();
        graph_3.removeAllSeries();
        graph_4.removeAllSeries();
//        graph_5.removeAllSeries();
//        graph_6.removeAllSeries();
//        graph_7.removeAllSeries();
//        graph_8.removeAllSeries();

        //series = new LineGraphSeries[8];

        series_1 = new LineGraphSeries<DataPoint>();
        series_2 = new LineGraphSeries<DataPoint>();
        series_3 = new LineGraphSeries<DataPoint>();
        series_4 = new LineGraphSeries<DataPoint>();
//        series_5 = new LineGraphSeries<DataPoint>();
//        series_6 = new LineGraphSeries<DataPoint>();
//        series_7 = new LineGraphSeries<DataPoint>();
//        series_8 = new LineGraphSeries<DataPoint>();

        //series_1.setColor(Color.RED);
        //series_2.setColor(Color.BLUE);


        //graph_1.addSeries(series[0]);graph_2.addSeries(series[1]);graph_3.addSeries(series[2]);graph_4.addSeries(series[3]);
        //graph_5.addSeries(series[4]);graph_6.addSeries(series[5]);graph_7.addSeries(series[6]);graph_8.addSeries(series[7]);

        graph_1.addSeries(series_1);graph_2.addSeries(series_2);graph_3.addSeries(series_3);graph_4.addSeries(series_4);
        //graph_5.addSeries(series_5);graph_6.addSeries(series_6);graph_7.addSeries(series_7);graph_8.addSeries(series_8);




        graph_1.getGridLabelRenderer().setVerticalAxisTitle("Level");
        graph_1.getGridLabelRenderer().setPadding(50);
        graph_1.getGridLabelRenderer().setLabelHorizontalHeight(50);

        graph_2.getGridLabelRenderer().setVerticalAxisTitle("Level");
        graph_2.getGridLabelRenderer().setPadding(50);
        graph_2.getGridLabelRenderer().setLabelHorizontalHeight(50);

        graph_3.getGridLabelRenderer().setVerticalAxisTitle("Level");
        graph_3.getGridLabelRenderer().setPadding(50);
        graph_3.getGridLabelRenderer().setLabelHorizontalHeight(50);

        graph_4.getGridLabelRenderer().setVerticalAxisTitle("Level");
        graph_4.getGridLabelRenderer().setPadding(50);
        graph_4.getGridLabelRenderer().setLabelHorizontalHeight(50);

        /*
        graph_5.getGridLabelRenderer().setVerticalAxisTitle("Level");
        graph_5.getGridLabelRenderer().setPadding(50);
        graph_5.getGridLabelRenderer().setLabelHorizontalHeight(50);

        graph_6.getGridLabelRenderer().setVerticalAxisTitle("Level");
        graph_6.getGridLabelRenderer().setPadding(50);
        graph_6.getGridLabelRenderer().setLabelHorizontalHeight(50);

        graph_7.getGridLabelRenderer().setVerticalAxisTitle("Level");
        graph_7.getGridLabelRenderer().setPadding(50);
        graph_7.getGridLabelRenderer().setLabelHorizontalHeight(50);

        graph_8.getGridLabelRenderer().setVerticalAxisTitle("Level");
        graph_8.getGridLabelRenderer().setPadding(50);
        graph_8.getGridLabelRenderer().setLabelHorizontalHeight(50);*/


        Viewport viewport = graph_1.getViewport();
        viewport.setXAxisBoundsManual(true);
        viewport.setMinX(0);
        viewport.setMaxX(5);
        viewport.setYAxisBoundsManual(true);
        viewport.setMinY(yAxisMin);
        viewport.setMaxY(yAxisMax);

        viewport = graph_2.getViewport();
        viewport.setXAxisBoundsManual(true);
        viewport.setMinX(0);
        viewport.setMaxX(5);
        viewport.setYAxisBoundsManual(true);
        viewport.setMinY(yAxisMin);
        viewport.setMaxY(yAxisMax);

        viewport = graph_3.getViewport();
        viewport.setXAxisBoundsManual(true);
        viewport.setMinX(0);
        viewport.setMaxX(5);
        viewport.setYAxisBoundsManual(true);
        viewport.setMinY(yAxisMin);
        viewport.setMaxY(yAxisMax);

        viewport = graph_4.getViewport();
        viewport.setXAxisBoundsManual(true);
        viewport.setMinX(0);
        viewport.setMaxX(5);
        viewport.setYAxisBoundsManual(true);
        viewport.setMinY(yAxisMin);
        viewport.setMaxY(yAxisMax);

        /*
        viewport = graph_5.getViewport();
        viewport.setXAxisBoundsManual(true);
        viewport.setMinX(0);
        viewport.setMaxX(5);
        viewport.setYAxisBoundsManual(true);
        viewport.setMinY(yAxisMin);
        viewport.setMaxY(yAxisMax);

        viewport = graph_6.getViewport();
        viewport.setXAxisBoundsManual(true);
        viewport.setMinX(0);
        viewport.setMaxX(5);
        viewport.setYAxisBoundsManual(true);
        viewport.setMinY(yAxisMin);
        viewport.setMaxY(yAxisMax);

        viewport = graph_7.getViewport();
        viewport.setXAxisBoundsManual(true);
        viewport.setMinX(0);
        viewport.setMaxX(5);
        viewport.setYAxisBoundsManual(true);
        viewport.setMinY(yAxisMin);
        viewport.setMaxY(yAxisMax);

        viewport = graph_8.getViewport();
        viewport.setXAxisBoundsManual(true);
        viewport.setMinX(0);
        viewport.setMaxX(5);
        viewport.setYAxisBoundsManual(true);
        viewport.setMinY(yAxisMin);
        viewport.setMaxY(yAxisMax);*/

        graph_1.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    return String.format("%d", (int) value);
                } else {
                    return super.formatLabel(value, isValueX);
                }
            }
        });

        graph_2.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    return String.format("%d", (int) value);
                } else {
                    return super.formatLabel(value, isValueX);
                }
            }
        });

        graph_3.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    return String.format("%d", (int) value);
                } else {
                    return super.formatLabel(value, isValueX);
                }
            }
        });

        graph_4.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    return String.format("%d", (int) value);
                } else {
                    return super.formatLabel(value, isValueX);
                }
            }
        });

        /*
        graph_5.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    return String.format("%d", (int) value);
                } else {
                    return super.formatLabel(value, isValueX);
                }
            }
        });

        graph_6.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    return String.format("%d", (int) value);
                } else {
                    return super.formatLabel(value, isValueX);
                }
            }
        });

        graph_7.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    return String.format("%d", (int) value);
                } else {
                    return super.formatLabel(value, isValueX);
                }
            }
        });

        graph_8.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    return String.format("%d", (int) value);
                } else {
                    return super.formatLabel(value, isValueX);
                }
            }
        });*/

    }

}
