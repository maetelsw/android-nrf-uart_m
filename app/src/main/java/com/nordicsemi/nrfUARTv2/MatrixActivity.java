package com.nordicsemi.nrfUARTv2;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class MatrixActivity extends Activity {
    public static final String TAG = "nRFUART_MATRIX";

    private static final int NUM_MATIRX = 16;   // 4 by 4
    private static final int NUM_CHANNEL = 8;

    public static TextView[] mMatrix;
    public static TextView[] mChannel;
    private MainActivity mMainActivity = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matrix);

        mMainActivity = new MainActivity();
        setGUI();
        //startUpdateChannel();
    }

    /*
    public void startUpdateChannel() {
        String channelString;

        Log.d(TAG, "suc");
        for (int i = 0; i < NUM_CHANNEL; i++) {
            channelString = String.format("%04d", mMainActivity.ch[i]);
            mChannel[i].setText(channelString);
        }
    }
    */


    public void setGUI() {
        mMatrix = new TextView[NUM_MATIRX];
        mChannel = new TextView[NUM_CHANNEL];

        /* mMatrix[0] = (TextView) findViewById(R.id.textView_M01);
        mMatrix[1] = (TextView) findViewById(R.id.textView_M02);
        mMatrix[2] = (TextView) findViewById(R.id.textView_M03);
        mMatrix[3] = (TextView) findViewById(R.id.textView_M04);
        mMatrix[4] = (TextView) findViewById(R.id.textView_M05);
        mMatrix[5] = (TextView) findViewById(R.id.textView_M06);
        mMatrix[6] = (TextView) findViewById(R.id.textView_M07);
        mMatrix[7] = (TextView) findViewById(R.id.textView_M08);
        mMatrix[8] = (TextView) findViewById(R.id.textView_M09);
        mMatrix[9] = (TextView) findViewById(R.id.textView_M10);
        mMatrix[10] = (TextView) findViewById(R.id.textView_M11);
        mMatrix[11] = (TextView) findViewById(R.id.textView_M12);
        mMatrix[12] = (TextView) findViewById(R.id.textView_M13);
        mMatrix[13] = (TextView) findViewById(R.id.textView_M14);
        mMatrix[14] = (TextView) findViewById(R.id.textView_M15);
        mMatrix[15] = (TextView) findViewById(R.id.textView_M16); */

        for (int i = 0; i < NUM_MATIRX; i++) {
            int resID;

            // "com.nordicsemi.nrfUARTv2:id/textView_M01"
            if (i < 9) {    // M01 ~ M09
                resID = getResources().getIdentifier("com.nordicsemi.nrfUARTv2:id/textView_M0" + (i + 1), null, null);
            } else {          // M10 ~ M16
                resID = getResources().getIdentifier("com.nordicsemi.nrfUARTv2:id/textView_M" + (i + 1), null, null);
            }

            mMatrix[i] = (TextView) findViewById(resID);
        }

        for (int i = 0; i < NUM_CHANNEL; i++) {
            int resID;

            // "com.nordicsemi.nrfUARTv2:id/textView_CH01"
            resID = getResources().getIdentifier("com.nordicsemi.nrfUARTv2:id/textView_CH0" + (i + 1), null, null);

            mChannel[i] = (TextView) findViewById(resID);

//            String channelString = String.format("%04d", mMainActivity.ch[i]);
//            mChannel[i].setText(channelString);
            //mChannel[i].setText("ch_" + mMainActivity.ch[i]);
        }
    }
}
