
/*
 * Copyright (c) 2015, Nordic Semiconductor
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.nordicsemi.nrfUARTv2;


import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.Date;

public class MainActivity extends Activity {
    public static final String TAG = "nRFUART";

    private static final int REQUEST_SELECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int UART_PROFILE_READY = 10;
    private static final int UART_PROFILE_CONNECTED = 20;
    private static final int UART_PROFILE_DISCONNECTED = 21;
    private static final int STATE_OFF = 10;


    private int mState = UART_PROFILE_DISCONNECTED;
    private UartService mService = null;
    private BluetoothDevice mDevice = null;
    private BluetoothAdapter mBtAdapter = null;
    private ListView messageListView;
    private ArrayAdapter<String> listAdapter;
    private Button btnConnectDisconnect, btnSend, btnMatrix, btnGraph;
    private EditText edtMessage;
    private MatrixActivity mMatrixActivity;

    private Handler mHandler;

    private int colorValue[] = new int[16];

    public static int ch[] = {0,};
    public static int nTmp[] = {0,};
    private int lastX = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBtAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        messageListView = (ListView) findViewById(R.id.listMessage);
        listAdapter = new ArrayAdapter<String>(this, R.layout.message_detail);
        messageListView.setAdapter(listAdapter);
        messageListView.setDivider(null);
        btnConnectDisconnect = (Button) findViewById(R.id.btn_select);
        btnSend = (Button) findViewById(R.id.sendButton);
        edtMessage = (EditText) findViewById(R.id.sendText);
        btnMatrix = (Button) findViewById(R.id.btn_matrix);
        //btnGraph = (Button) findViewById(R.id.btn_graph);

        ch = new int[8];      // 8-channels
        nTmp = new int[16];     // 16-BYTES to integer

        service_init();


        for (int i = 0; i < 16; i++) {
            colorValue[i] = 255;
        }

        // Alter to Graph
        /*btnGraph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, GraphActivity.class);
                startActivity(intent);
            }
        });*/

        // Alter to MatrixActivity
        btnMatrix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MatrixActivity.class);
                startActivity(intent);
            }
        });

        // Handle Disconnect & Connect button
        btnConnectDisconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mBtAdapter.isEnabled()) {
                    Log.i(TAG, "onClick - BT not enabled yet");
                    Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
                } else {
                    if (btnConnectDisconnect.getText().equals("Connect")) {

                        //Connect button pressed, open DeviceListActivity class, with popup windows that scan for devices

                        Intent newIntent = new Intent(MainActivity.this, DeviceListActivity.class);
                        startActivityForResult(newIntent, REQUEST_SELECT_DEVICE);
                    } else {
                        //Disconnect button pressed
                        if (mDevice != null) {
                            mService.disconnect();
                        }
                    }
                }
            }
        });

        // Handle Send button
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editText = (EditText) findViewById(R.id.sendText);
                String message = editText.getText().toString();
                byte[] value;
                try {
                    //send data to service
                    value = message.getBytes("UTF-8");
                    mService.writeRXCharacteristic(value);
                    //Update the log with time stamp
                    String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                    listAdapter.add("[" + currentDateTimeString + "] TX: " + message);
                    messageListView.smoothScrollToPosition(listAdapter.getCount() - 1);
                    edtMessage.setText("");
                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * UART service connected/disconnected
     * <p>
     * In nRF5_SDK, 'ble_nus_put' or 'ble_nus_get'
     */
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            mService = ((UartService.LocalBinder) rawBinder).getService();
            Log.d(TAG, "onServiceConnected mService= " + mService);
            if (!mService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
        }

        public void onServiceDisconnected(ComponentName classname) {
            ////     mService.disconnect(mDevice);
            mService = null;
        }
    };


    private final BroadcastReceiver UARTStatusChangeReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, final Intent intent) {
            String action = intent.getAction();


            final Intent mIntent = intent;

            //*********************//
            if (action.equals(UartService.ACTION_GATT_CONNECTED)) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                        Log.d(TAG, "UART_CONNECT_MSG");
                        btnConnectDisconnect.setText("Disconnect");
                        edtMessage.setEnabled(true);
                        btnSend.setEnabled(true);
                        ((TextView) findViewById(R.id.deviceName)).setText(mDevice.getName() + " - ready");
                        listAdapter.add("[" + currentDateTimeString + "] Connected to: " + mDevice.getName());
                        messageListView.smoothScrollToPosition(listAdapter.getCount() - 1);
                        mState = UART_PROFILE_CONNECTED;
                    }
                });
            }

            //*********************//
            if (action.equals(UartService.ACTION_GATT_DISCONNECTED)) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                        Log.d(TAG, "UART_DISCONNECT_MSG");
                        btnConnectDisconnect.setText("Connect");
                        edtMessage.setEnabled(false);
                        btnSend.setEnabled(false);
                        ((TextView) findViewById(R.id.deviceName)).setText("Not Connected");
                        listAdapter.add("[" + currentDateTimeString + "] Disconnected to: " + mDevice.getName());
                        mState = UART_PROFILE_DISCONNECTED;
                        mService.close();
                        //setUiState();

                    }
                });
            }


            //*********************//
            if (action.equals(UartService.ACTION_GATT_SERVICES_DISCOVERED)) {
                mService.enableTXNotification();
            }

            //*********************//
            final int thresholdChannel[] = new int[8];

            // Horizontal : Bottom
            thresholdChannel[0] = 3000;         // ch01-H1
            thresholdChannel[1] = 2860;         // ch02-H2
            thresholdChannel[2] = 3300;         // ch02-H3
            thresholdChannel[3] = 2710;         // ch04-H4

            // Vertical : Top
            thresholdChannel[4] = 3050;         // ch05-V1
            thresholdChannel[5] = 3000;         // ch06-V2
            thresholdChannel[6] = 2520;         // ch07-V3
            thresholdChannel[7] = 2820;         // ch08-V4


            if (action.equals(UartService.ACTION_DATA_AVAILABLE)) {
                final byte[] txValue = intent.getByteArrayExtra(UartService.EXTRA_DATA);           // java not supports 'unsigned'

                runOnUiThread(new Runnable() {
                    public void run() {
                        try {

                            /* Algorithm #2 */
                            String channelString;
                            int flagOnMatrix[][] = new int[4][4];
                            for (int i = 0; i < 4; i++) {
                                for (int j = 0; j < 4; j++) {
                                    flagOnMatrix[i][j] = 0;
                                }
                            }

                            // Casting BYTE to INTEGER
                            for (int i = 0; i < 16; i++) {
                                if (txValue[i] < 0)
                                    nTmp[i] = (int) txValue[i] + 256;
                                else
                                    nTmp[i] = (int) txValue[i];
                            }

                            // Invoking each channel value
                            for (int idx = 0; idx < 8; idx++) {
                                ch[idx] = nTmp[idx * 2] + (nTmp[idx * 2 + 1] << 8);

                                // cutting for maximum 4095
                                if (ch[idx] > 4095) {
                                    ch[idx] = 4095;
                                }


                                /**
                                 * Adjust to Threshold Algorithm By Jees
                                 */
                                // each channels for checking to each threshold
                                if (ch[idx] > thresholdChannel[idx]) {                                       // Released
                                    channelString = String.format("%04d", ch[idx]);
                                    mMatrixActivity.mChannel[idx].setText(channelString);
                                    //mMatrixActivity.mChannel[idx].setBackgroundColor(Color.WHITE);
                                } else {                                                                     // Pressed !!
                                    channelString = String.format("%04d", ch[idx]);
                                    mMatrixActivity.mChannel[idx].setText(channelString);
                                    //mMatrixActivity.mChannel[idx].setBackgroundColor(Color.RED);


                                    // option #1. Flagging first to Bottom-Horizontal
                                    if (idx < 4) {  // First, flagging by Horizontal(Bottom)
                                        for (int i = 0; i < 4; i++) {
                                            //Log.d(TAG, "fg[idx][] "+idx +","+i);
                                            flagOnMatrix[idx][i] += 1;

                                        }
                                    } else {    // Second, flagging by Vertical(Top)
                                        for (int k = 0; k < 4; k++) {
                                            flagOnMatrix[k][idx - 4] += 1;
                                        }
                                    }
                                } // Pressed...
                            }

                            // Update mapping matrix
                            //Log.d(TAG,"BF-mat");
                            for (int i = 0; i < 4; i++) {
                                for (int j = 0; j < 4; j++) {
                                    //Log.d(TAG, "valMat["+i+"]"+"["+j+"] "+flagOnMatrix[i][j]);
                                    if (flagOnMatrix[i][j] > 1) {
                                        if (i == 0 && j == 0) {
                                            if (colorValue[0] < 10)
                                                colorValue[0] = 0;
                                            else
                                                colorValue[0] -= 10;

                                            mMatrixActivity.mMatrix[0].setBackgroundColor(Color.rgb(255, colorValue[0], colorValue[0]));
                                            Log.d(TAG, "colvaluered: " + colorValue[0]);
                                        } else if (i == 0 && j == 1) {
                                            if (colorValue[1] < 10)
                                                colorValue[1] = 0;
                                            else
                                                colorValue[1] -= 10;

                                            mMatrixActivity.mMatrix[1].setBackgroundColor(Color.rgb(255, colorValue[1], colorValue[1]));
                                        } else if (i == 0 && j == 2) {
                                            if (colorValue[2] < 10)
                                                colorValue[2] = 0;
                                            else
                                                colorValue[2] -= 10;

                                            mMatrixActivity.mMatrix[2].setBackgroundColor(Color.rgb(255, colorValue[2], colorValue[2]));
                                        } else if (i == 0 && j == 3) {
                                            if (colorValue[3] < 10)
                                                colorValue[3] = 0;
                                            else
                                                colorValue[3] -= 10;

                                            mMatrixActivity.mMatrix[3].setBackgroundColor(Color.rgb(255, colorValue[3], colorValue[3]));
                                        } else if (i == 1 && j == 0) {
                                            if (colorValue[4] < 10)
                                                colorValue[4] = 0;
                                            else
                                                colorValue[4] -= 10;

                                            mMatrixActivity.mMatrix[4].setBackgroundColor(Color.rgb(255, colorValue[4], colorValue[4]));
                                        } else if (i == 1 && j == 1) {
                                            if (colorValue[5] < 10)
                                                colorValue[5] = 0;
                                            else
                                                colorValue[5] -= 10;

                                            mMatrixActivity.mMatrix[5].setBackgroundColor(Color.rgb(255, colorValue[5], colorValue[5]));
                                        } else if (i == 1 && j == 2) {
                                            if (colorValue[6] < 10)
                                                colorValue[6] = 0;
                                            else
                                                colorValue[6] -= 10;

                                            mMatrixActivity.mMatrix[6].setBackgroundColor(Color.rgb(255, colorValue[6], colorValue[6]));
                                        } else if (i == 1 && j == 3) {
                                            if (colorValue[7] < 10)
                                                colorValue[7] = 0;
                                            else
                                                colorValue[7] -= 10;

                                            mMatrixActivity.mMatrix[7].setBackgroundColor(Color.rgb(255, colorValue[7], colorValue[7]));
                                        } else if (i == 2 && j == 0) {
                                            if (colorValue[8] < 10)
                                                colorValue[8] = 0;
                                            else
                                                colorValue[8] -= 10;

                                            mMatrixActivity.mMatrix[8].setBackgroundColor(Color.rgb(255, colorValue[8], colorValue[8]));
                                        } else if (i == 2 && j == 1) {
                                            if (colorValue[9] < 10)
                                                colorValue[9] = 0;
                                            else
                                                colorValue[9] -= 10;

                                            mMatrixActivity.mMatrix[9].setBackgroundColor(Color.rgb(255, colorValue[9], colorValue[9]));
                                        } else if (i == 2 && j == 2) {
                                            if (colorValue[10] < 10)
                                                colorValue[10] = 0;
                                            else
                                                colorValue[10] -= 10;

                                            mMatrixActivity.mMatrix[10].setBackgroundColor(Color.rgb(255, colorValue[10], colorValue[10]));
                                        } else if (i == 2 && j == 3) {
                                            if (colorValue[11] < 10)
                                                colorValue[11] = 0;
                                            else
                                                colorValue[11] -= 10;

                                            mMatrixActivity.mMatrix[11].setBackgroundColor(Color.rgb(255, colorValue[11], colorValue[11]));
                                        } else if (i == 3 && j == 0) {
                                            if (colorValue[12] < 10)
                                                colorValue[12] = 0;
                                            else
                                                colorValue[12] -= 10;

                                            mMatrixActivity.mMatrix[12].setBackgroundColor(Color.rgb(255, colorValue[12], colorValue[12]));
                                        } else if (i == 3 && j == 1) {
                                            if (colorValue[13] < 10)
                                                colorValue[13] = 0;
                                            else
                                                colorValue[13] -= 10;

                                            mMatrixActivity.mMatrix[13].setBackgroundColor(Color.rgb(255, colorValue[13], colorValue[13]));
                                        } else if (i == 3 && j == 2) {
                                            if (colorValue[14] < 10)
                                                colorValue[14] = 0;
                                            else
                                                colorValue[14] -= 10;

                                            mMatrixActivity.mMatrix[14].setBackgroundColor(Color.rgb(255, colorValue[14], colorValue[14]));
                                        } else if (i == 3 && j == 3) {
                                            if (colorValue[15] < 10)
                                                colorValue[15] = 0;
                                            else
                                                colorValue[15] -= 10;

                                            mMatrixActivity.mMatrix[15].setBackgroundColor(Color.rgb(255, colorValue[15], colorValue[15]));
                                        }
                                    } else if (flagOnMatrix[i][j] == 0) {

                                        if (i == 0 && j == 0) {
                                            colorValue[0] = 255;
                                            mMatrixActivity.mMatrix[0].setBackgroundColor(Color.WHITE);
                                        } else if (i == 0 && j == 1) {
                                            colorValue[1] = 255;
                                            mMatrixActivity.mMatrix[1].setBackgroundColor(Color.WHITE);
                                        } else if (i == 0 && j == 2) {
                                            colorValue[2] = 255;
                                            mMatrixActivity.mMatrix[2].setBackgroundColor(Color.WHITE);
                                        } else if (i == 0 && j == 3) {
                                            colorValue[3] = 255;
                                            mMatrixActivity.mMatrix[3].setBackgroundColor(Color.WHITE);
                                        } else if (i == 1 && j == 0) {
                                            colorValue[4] = 255;
                                            mMatrixActivity.mMatrix[4].setBackgroundColor(Color.WHITE);
                                        } else if (i == 1 && j == 1) {
                                            colorValue[5] = 255;
                                            mMatrixActivity.mMatrix[5].setBackgroundColor(Color.WHITE);
                                        } else if (i == 1 && j == 2) {
                                            colorValue[6] = 255;
                                            mMatrixActivity.mMatrix[6].setBackgroundColor(Color.WHITE);
                                        } else if (i == 1 && j == 3) {
                                            colorValue[7] = 255;
                                            mMatrixActivity.mMatrix[7].setBackgroundColor(Color.WHITE);
                                        } else if (i == 2 && j == 0) {
                                            colorValue[8] = 255;
                                            mMatrixActivity.mMatrix[8].setBackgroundColor(Color.WHITE);
                                        } else if (i == 2 && j == 1) {
                                            colorValue[9] = 255;
                                            mMatrixActivity.mMatrix[9].setBackgroundColor(Color.WHITE);
                                        } else if (i == 2 && j == 2) {
                                            colorValue[10] = 255;
                                            mMatrixActivity.mMatrix[10].setBackgroundColor(Color.WHITE);
                                        } else if (i == 2 && j == 3) {
                                            colorValue[11] = 255;
                                            mMatrixActivity.mMatrix[11].setBackgroundColor(Color.WHITE);
                                        } else if (i == 3 && j == 0) {
                                            colorValue[12] = 255;
                                            mMatrixActivity.mMatrix[12].setBackgroundColor(Color.WHITE);
                                        } else if (i == 3 && j == 1) {
                                            colorValue[13] = 255;
                                            mMatrixActivity.mMatrix[13].setBackgroundColor(Color.WHITE);
                                        } else if (i == 3 && j == 2) {
                                            colorValue[14] = 255;
                                            mMatrixActivity.mMatrix[14].setBackgroundColor(Color.WHITE);
                                        } else if (i == 3 && j == 3) {
                                            colorValue[15] = 255;
                                            mMatrixActivity.mMatrix[15].setBackgroundColor(Color.WHITE);
                                        }
                                    }
                                }
                            }
                            // Log.d(TAG,"AF-mat");


                            // show on GraphActivity
                            /*mGraphActivity.series_1.appendData(new DataPoint(lastX, ch[0]), true, 400);
                            mGraphActivity.series_2.appendData(new DataPoint(lastX, ch[1]), true, 400);
                            mGraphActivity.series_3.appendData(new DataPoint(lastX, ch[2]), true, 400);
                            mGraphActivity.series_4.appendData(new DataPoint(lastX, ch[3]), true, 400);
                            lastX += 0.1; Log.d(TAG, "AF,appenGph");*/
                        } catch (Exception e) {
                            Log.e(TAG, e.toString());
                        }
                    }
                });
            }


            //*********************//
            if (action.equals(UartService.DEVICE_DOES_NOT_SUPPORT_UART)) {
                showMessage("Device doesn't support UART. Disconnecting");
                mService.disconnect();
            }
        }
    };

    private void service_init() {
        Intent bindIntent = new Intent(this, UartService.class);
        bindService(bindIntent, mServiceConnection, Context.BIND_AUTO_CREATE);      //
        LocalBroadcastManager.getInstance(this).registerReceiver(UARTStatusChangeReceiver, makeGattUpdateIntentFilter());
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(UartService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(UartService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(UartService.DEVICE_DOES_NOT_SUPPORT_UART);
        return intentFilter;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");

        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(UARTStatusChangeReceiver);
        } catch (Exception ignore) {
            Log.e(TAG, ignore.toString());
        }
        unbindService(mServiceConnection);
        mService.stopSelf();
        mService = null;
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        if (!mBtAdapter.isEnabled()) {
            Log.i(TAG, "onResume - BT not enabled yet");
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }


    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_SELECT_DEVICE:
                //When the DeviceListActivity return, with the selected device address
                if (resultCode == Activity.RESULT_OK && data != null) {
                    String deviceAddress = data.getStringExtra(BluetoothDevice.EXTRA_DEVICE);
                    mDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(deviceAddress);

                    Log.d(TAG, "... onActivityResultdevice.address==" + mDevice + "mserviceValue" + mService);
                    ((TextView) findViewById(R.id.deviceName)).setText(mDevice.getName() + " - connecting");
                    mService.connect(deviceAddress);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    Toast.makeText(this, "Bluetooth has turned on ", Toast.LENGTH_SHORT).show();
                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "BT not enabled");
                    Toast.makeText(this, "Problem in BT Turning ON ", Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
            default:
                Log.e(TAG, "wrong request code");
                break;
        }
    }

    private void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        if (mState == UART_PROFILE_CONNECTED) {
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
            showMessage("nRFUART's running in background.\n             Disconnect to exit");
        } else {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(R.string.popup_title)
                    .setMessage(R.string.popup_message)
                    .setPositiveButton(R.string.popup_yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton(R.string.popup_no, null)
                    .show();
        }
    }
}
